<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <meta name="Description" content="Enter your description here" />
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/4.6.0/css/bootstrap.min.css">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.15.4/css/all.min.css">
    <link rel="stylesheet" href="assets/css/style.css">
    <title>Power Energy Charge Rate</title>
</head>

<body>

    <div class="row">
        <div class="col-md-3"></div>
        <div class="col-md-6">

            <form method="post" action="" style='margin:auto; width:700px'>
                <h1>CALCULATE</h1>
                <hr>

                <div class="form-group">
                    <label for="voltage">Voltage</label>
                    <input type="number" min="1" step="any" class="form-control" id="voltage" name="voltage"
                        aria-describedby="voltage(V)">
                    <small id="voltage(V)" class="form-text text-muted">Voltage(V)</small><br>

                    <label for="current">Current</label>
                    <input type="number" min="1" step="any" class="form-control" id="current" name="current"
                        aria-describedby="ampere(A)">
                    <small id="ampere(A)" class="form-text text-muted">Ampere(A)</small><br>

                    <label for="rate">CURRENT RATE</label>
                    <input type="number" min="1" step="any" class="form-control" id="rate" name="rate"
                        aria-describedby="currentrate">
                    <small id="currentrate" class="form-text text-muted">sen/kWh</small>
                </div>

                <button type="submit" class="btn btn-primary" name='btn'>Calculate</button>
            </form>

            <table class="table table-striped">
                <thead>
                    <hr>
                    <tr>
                        <th>Bil</th>
                        <th>Hour</th>
                        <th>Energy(kWh)</th>
                        <th>Total(Rm)</th>
                    </tr>
                </thead>


                <tbody>
                    <?php
                    if (isset($_POST['btn'])) {
                        $voltage = $_POST['voltage'];
                        $current = $_POST['current'];
                        $currentrate = $_POST['rate'];

                        $power = $voltage * $current / 1000;
                        $rate = $currentrate / 100;

                        echo "Voltage = $voltage kw <br>";
                        echo "Current = $current A <br>";
                        echo "Current Rate = Rm $currentrate<br>";
                        echo "<hr>";
                        echo "Power = $power kw <br>";
                        echo "Rate = $rate Rm <br>";
                        echo "<hr>";

                        for ($HourBil = 1; $HourBil <= 24; $HourBil++) {
                            $energy = $power * $HourBil * 1000;
                            $total = $energy * ($currentrate / 100);
                            $rounded_value = round($total, 2);
                            echo "<tr>";
                            echo "<td>$HourBil</td>";
                            echo "<td>$HourBil</td>";
                            echo "<td>$energy</td>";
                            echo "<td>$rounded_value</td>";
                            echo "</tr>";
                        }
                    }

                    ?>
            </table>
            <table class="table table-striped">
                <thead>
                    <hr>
                    <tr>
                        <th>Bil</th>
                        <th>Day</th>
                        <th>Energy(kWh)</th>
                        <th>Total(Rm)</th>
                    </tr>
                </thead>
                <tbody>
                    <?php
                    $width = 200;
                    if (isset($_POST['btn'])) {
                        $voltage = $_POST['voltage'];
                        $current = $_POST['current'];
                        $currentrate = $_POST['rate'];

                        $power = $voltage * $current / 1000;
                        $rate = $currentrate / 100;

                        echo "Voltage = $voltage kw <br>";
                        echo "Current = $current A <br>";
                        echo "Current Rate = Rm $currentrate<br>";
                        echo "<hr>";
                        echo "Power = $power kw <br>";
                        echo "Rate = $rate Rm <br>";
                        echo "<hr>";
                            for ($HourBil = 24, $Day = 1; $HourBil <= 744 && $Day <= 36; $HourBil+=24, $Day++) {
                                $energy = $power * $HourBil * 1000;
                                $total = $energy * ($currentrate / 100);
                                $rounded_value = round($total, 2);
                                echo "<tr>";
                                echo "<td>$Day</td>";
                                echo "<td>$Day</td>";
                                echo "<td>$energy</td>";
                                echo "<td>$rounded_value</td>";
                                echo "</tr>";
                        }
                    }

                    ?>

            </table>

        </div>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.5.1/jquery.slim.min.js"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.16.1/umd/popper.min.js"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/4.6.0/js/bootstrap.min.js"></script>
</body>

</html>